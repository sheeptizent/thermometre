package Termometre;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.*;

public class TempViewButtonFahrenheit extends View {
	private static final long serialVersionUID = 2883635233521488481L;
	JTextField tempDisplay;
	JButton upButton, downButton;

	public TempViewButtonFahrenheit(String label, Model tm, Presenter tp) {
		super(label, tm, tp);
		tempDisplay = new JTextField();
		tempDisplay.setText(" " + mySubject.getTempFahrenheit());
		ActionListenerDisplayTempViewButtonFahrenheit a = new ActionListenerDisplayTempViewButtonFahrenheit(this);
		tempDisplay.addActionListener(a);

		upButton = new JButton("+");
		ActionListenerPlusTempViewButtonFahrenheit b = new ActionListenerPlusTempViewButtonFahrenheit(this);
		upButton.addActionListener(b);

		downButton = new JButton("-");
		ActionListenerMoinsTempViewButtonFahrenheit c = new ActionListenerMoinsTempViewButtonFahrenheit(this);
		downButton.addActionListener(c);
		JPanel panelButtons = new JPanel();
		panelButtons.add(downButton);
		panelButtons.add(upButton);

		add(tempDisplay, BorderLayout.CENTER);
		add(panelButtons, BorderLayout.SOUTH);
		revalidate();

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		tempDisplay.setText(" " + mySubject.getTempFahrenheit());
		//System.out.println("update " + mySubject.getTempFahrenheit());
	}

}

class ActionListenerDisplayTempViewButtonFahrenheit implements ActionListener {
	TempViewButtonFahrenheit myView;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		float t = 0;
		try {
			t = Float.valueOf(myView.tempDisplay.getText());
		} catch (Exception e) {
		}
		myView.myPresenter.setFahrenheit(t);
		System.out.println("temp " + myView.mySubject.getTempFahrenheit());
	}

	public ActionListenerDisplayTempViewButtonFahrenheit(TempViewButtonFahrenheit tvfb) {
		myView = tvfb;
	}
}

class ActionListenerPlusTempViewButtonFahrenheit implements ActionListener {
	TempViewButtonFahrenheit myView;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		myView.myPresenter.plusFaherenheit();
		System.out.println("temp " + myView.mySubject.getTempFahrenheit());
	}

	public ActionListenerPlusTempViewButtonFahrenheit(TempViewButtonFahrenheit tvfb) {
		myView = tvfb;
	}
}

class ActionListenerMoinsTempViewButtonFahrenheit implements ActionListener {
	TempViewButtonFahrenheit myView;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		myView.myPresenter.moinsFaherenheit();
		System.out.println("temp " + myView.mySubject.getTempFahrenheit());
	}

	public ActionListenerMoinsTempViewButtonFahrenheit(TempViewButtonFahrenheit tvfb) {
		myView = tvfb;
	}
}
