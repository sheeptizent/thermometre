package Termometre;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.*;

public class TempViewButtonCelsius extends View {
	private static final long serialVersionUID = 2883635233521488480L;
	JTextField tempDisplay;
	JButton upButton, downButton;

	public TempViewButtonCelsius(String label, Model tm, Presenter tp) {
		super(label, tm, tp);
		tempDisplay = new JTextField();
		tempDisplay.setText(" " + mySubject.getTempCelsius());
		ActionListenerDisplayTempViewButtonCelsius a = new ActionListenerDisplayTempViewButtonCelsius(this);
		tempDisplay.addActionListener(a);

		upButton = new JButton("+");
		ActionListenerPlusTempViewButtonCelsius b = new ActionListenerPlusTempViewButtonCelsius(this);
		upButton.addActionListener(b);

		downButton = new JButton("-");
		ActionListenerMoinsTempViewButtonCelsius c = new ActionListenerMoinsTempViewButtonCelsius(this);
		downButton.addActionListener(c);
		JPanel panelButtons = new JPanel();
		panelButtons.add(downButton);
		panelButtons.add(upButton);

		add(tempDisplay, BorderLayout.CENTER);
		add(panelButtons, BorderLayout.SOUTH);
		revalidate();
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		tempDisplay.setText(" " + mySubject.getTempCelsius());
		//System.out.println("update " + mySubject.getTempCelsius());
	}

}

class ActionListenerDisplayTempViewButtonCelsius implements ActionListener {
	TempViewButtonCelsius myView;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		float t = 0;
		try {
			t = Float.valueOf(myView.tempDisplay.getText());
		} catch (Exception e) {
		}
		myView.myPresenter.setCelsius(t);
		System.out.println("temp " + myView.mySubject.getTempCelsius());
	}

	public ActionListenerDisplayTempViewButtonCelsius(TempViewButtonCelsius tvcb) {
		myView = tvcb;
	}
}

class ActionListenerPlusTempViewButtonCelsius implements ActionListener {
	TempViewButtonCelsius myView;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		myView.myPresenter.plusCelsius();
		System.out.println("temp " + myView.mySubject.getTempCelsius());
	}

	public ActionListenerPlusTempViewButtonCelsius(TempViewButtonCelsius tvcb) {
		myView = tvcb;
	}
}

class ActionListenerMoinsTempViewButtonCelsius implements ActionListener {
	TempViewButtonCelsius myView;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		myView.myPresenter.moinsCelsius();
		System.out.println("temp " + myView.mySubject.getTempCelsius());
	}

	public ActionListenerMoinsTempViewButtonCelsius(TempViewButtonCelsius tvcb) {
		myView = tvcb;
	}
}
