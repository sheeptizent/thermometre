package Termometre;

import java.util.Observer;

import javax.swing.JFrame;

public abstract class View extends JFrame implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Model mySubject;
	Presenter myPresenter;

	public View(String label, Model tm, Presenter tp, int posX, int posY) {
		super(label);
		init(tm, tp, posX, posY);
	}

	public View(String label, Model tm, Presenter tp) {
		super(label);
		init(tm, tp, 0, 0);
	}

	private void init(Model tm, Presenter tp, int posX, int posY) {
		mySubject = tm;
		myPresenter = tp;
		setSize(200, 200);
		setLocation(posX, posY);
		setVisible(true);
	}

}