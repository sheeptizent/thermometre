package Termometre;

import java.util.Observable;

public class Model extends Observable {
	private float temperatureC;

	public float getTempCelsius() {
		return temperatureC;
	}

	public void setTempCelsius(float t) {
		
		temperatureC = t;
		setChanged();		
		notifyObservers();
		
	}

	public Model(float temperatureC) {
		super();
		this.temperatureC = temperatureC;
	}

	public float getTempFahrenheit() {
		return Presenter.convFahrenheit(temperatureC);
	}
}
