package Termometre;

public class ThermometreMVC {
	public static void main(String[] args) {
		Model tm = new Model(20);
		Presenter tp = new Presenter(tm);
		TempViewButtonCelsius tvc = new TempViewButtonCelsius("Vue Jerem", tm, tp);
		tm.addObserver(tvc);
		TempViewButtonFahrenheit tvf = new TempViewButtonFahrenheit("Vue Farenheit Jerem", tm, tp);
		tm.addObserver(tvf);

	}
}
