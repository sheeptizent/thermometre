package Termometre;

public class Presenter {
	Model monModel;

	public Presenter(Model tm) {
		
		monModel = tm;
	}

	public void plusCelsius() {
		float temp = monModel.getTempCelsius();
		temp++;
		monModel.setTempCelsius(temp);
	}

	public void moinsCelsius() {
		float temp = monModel.getTempCelsius();
		temp--;

		monModel.setTempCelsius(temp);
	}

	public void plusFaherenheit() {
		float temp = monModel.getTempFahrenheit();
		temp++;
		temp = convCelsius(temp);
		monModel.setTempCelsius(temp);
	}

	public void moinsFaherenheit() {
		float temp = monModel.getTempFahrenheit();
		temp--;
		temp = convCelsius(temp);
		monModel.setTempCelsius(temp);
	}

	public static float convFahrenheit(float f) {
		Float temp = (f * 9 / 5) + 32;
		return temp;
	}

	public static float convCelsius(float f) {

		Float temp = (f - 32) * 5 / 9;
		return temp;
	}

	public void setCelsius(float f) {
		monModel.setTempCelsius(f);
	}

	public void setFahrenheit(float f) {
		monModel.setTempCelsius(convCelsius(f));
	}
}
